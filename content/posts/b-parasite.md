---
title: "B-parasite moisture sensor"
date: 2022-12-08
---

[b-parasite](https://github.com/rbaron/b-parasite) is an excellent open hardware soil moisture sensor.

# Flashing firmware with Raspberry Pi Pico

Install [GNU Arm Toolchain](https://lindevs.com/install-arm-gnu-toolchain-on-ubuntu)

Install [nrf command line tools](https://www.nordicsemi.com/Products/Development-tools/nrf-command-line-tools/download). This is needed for `mergehex` later

Download `nRF5 SDK` and `SoftDevice S140` from [here](https://www.nordicsemi.com/Products/Development-software/nRF5-SDK/Download#infotabs)  

Extract SDK, in my case it was to `~/development/nRF5_SDK_17.1.0_ddde560`

Modify `~/development/nRF5_SDK_17.1.0_ddde560/components/toolchain/gcc/Makefile.posix` and set GNU_INSTALL_ROOT to the path where you've installed your GCC

```bash
git clone https://github.com/rbaron/b-parasite.git
cd code/b-parasite

# actual path to the extracted SDK
export SDK_ROOT=~/development/nRF5_SDK_17.1.0_ddde560
make

cd _build
cp <path-to-downloaded-s140_nrf52_7.2.0_softdevice.hex> s140_nrf52_7.2.0_softdevice.hex
mergehex -m s140_nrf52_7.2.0_softdevice.hex nrf52840_xxaa.hex -o out.hex
```

Install [picoprobe](https://github.com/raspberrypi/picoprobe) and OpenOCD by following PDF guide from Picoprobe's README

Open another tab and from `openocd` folder run:
```bash
src/openocd -f interface/cmsis-dap.cfg -f target/nrf52.cfg -s tcl
```

Go back to `b-parasite/code/b-parasite` and create a file `load.gdb` with the following content:

```bash
set confirm off
target extended-remote localhost:3333
load out.hex
detach
quit
```

Now you can flash `out.hex` with:
```bash
arm-none-eabi-gdb --command=load.gdb
```

If you disconnect the power from your b-parasite and reconnect it should briefly flash the led indicating that firmware has been loaded.

